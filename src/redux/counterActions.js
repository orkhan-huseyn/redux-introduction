export const INCREMENT = "increment";
export const DECREMENT = "decrement";
export const RESET = "reset";

export function increment() {
  return {
    type: INCREMENT,
  };
}

export function decrement() {
  return {
    type: DECREMENT,
  };
}

export function reset() {
  return {
    type: RESET,
  };
}
