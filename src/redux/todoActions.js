export const SET_TODOS = "set todos";

export function setTodos(todosArray) {
  return {
    type: SET_TODOS,
    payload: todosArray,
  };
}

export function fetchTodos() {
  return async function (dispatch) {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    const todoList = await response.json();
    dispatch(setTodos(todoList));
  };
}
