import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment, reset } from "./redux/counterActions";
import { fetchTodos } from "./redux/todoActions";

function App() {
  const dispatch = useDispatch();
  const counter = useSelector((state) => state.counter.value);
  const todos = useSelector((state) => state.todos.list);

  useEffect(() => {
    dispatch(fetchTodos());
  }, []);

  return (
    <>
      <h1>Counter: {counter}</h1>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <button onClick={() => dispatch(reset())}>Reset</button>
      <hr />
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>{todo.title}</li>
        ))}
      </ul>
    </>
  );
}

export default App;
